#DDIC#
##DI-контейнер##

### Примеры использования ###

```
#!coffeescript

container = new DDIC()

# Устанавливаем строковое значение
container.set( 'name', '...' );

# Устанавливаем объект
container.set( 'config', {
        host: '...'
});

# Создаём сервис возвращающий экземпляр пользователя
# Экземпляр будет создан только при первом обращении
container.set('user', ( container ) ->
    new User(container.get('name'));
)


# Переопределяем пользователя. Первый аргумент - то, что возвращает сервис user (в нашем случае экземпляр User), второй - контейнер
container.extend( 'user', ( userServiceOldResult , container ) ->
    new ProxyUser( userServiceOldResult ); # здесь ( userServiceOldResult ) мы можем делать что угодно с тем, что раньше возвращал сервис user
);


container.set('app', ( container ) ->
    new Application( container.get( 'config' ), container.get( 'user' ) );
);

# Можно как то поработав над уже существующим экземпляром, переопределить сервис новым результатом
container.extend('app', ( appServiseOldResult, container ) ->
    appServiseOldResult.setSomething();
    return appServiseOldResult;
);

# Получаем экземпляр приложения
app = container.get('app');

```