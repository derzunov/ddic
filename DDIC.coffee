###
The MIT License (MIT)

Copyright (c) 2015 Dmitry Erzunov (derzunov.work@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
###

###
#   DDIC DI CONTAINER
#   Created by Dmitry Erzunov (derzunov.work@gmail.com)
###
class DDIC
    constructor: () ->
        @values = {}
        @services = {}
 
    set: ( name, callback ) ->
        if (typeof callback == "function") 
            @services[name] = callback
         else 
            @values[name] = callback
        

    get: ( name ) ->
        if (@values[name] == undefined and typeof @services[name] == 'function')

            # передаём сам контейнер (@),
            # чтобы изнутри сервиса были доступны его значения и другие его сервисы (см. примеры )
            @values[name] = @services[name](@) # записываем занчение возвращаемое сервисом в значения контейнера
        
        return @values[name]
    
 
    extend: ( name, callback ) ->
        service = @services[name]
 
        if ( service == undefined )
            console.error  'Еhere is no ' + name + ' service in DDIC'

        @services[name] = (container) ->
            callback(service(container), container)

